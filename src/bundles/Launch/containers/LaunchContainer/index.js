import React, { Component} from 'react'
import Launch from '@Launch/components/Launch'
import {connect} from 'react-redux'
import {Actions} from 'react-native-router-flux'
import { bindActionCreators } from 'redux'
import { fetchList} from '@store/modules/auth/actions'

class LaunchContainer extends Component {

  render() {
    return (
      <Launch  {...this.props} obj={this}/>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    ListData : state.auth.ListData
  }
}

const mapDispatchToProps = (dispatch) => {
   return {
    fetchList: (params) => {dispatch(fetchList(params))}
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LaunchContainer)
