
import {StyleSheet, Dimensions} from 'react-native'

const styles = StyleSheet.create({
  container: {
    marginTop:76,
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-76,
  },

  btn: {
      height:35,
      backgroundColor: '#F57B20',
      alignSelf:'center',
      justifyContent: 'center',
      alignItems: 'center',
      width:200,
      marginTop:200,
  },
  
  textStyle:{
    marginTop:5,
    fontSize :16,
    fontWeight:'bold',
    height:30,
    color:'black'
  },

  CellStyle:{
    alignItems:'center',
    backgroundColor:'white',
    backgroundColor: '#F57B20',
    width: Dimensions.get('window').width-10,
    height:40,
    borderBottomWidth:1,
    borderBottomColor:'black',
    margin: 5
  },
})

export default styles
