import React from 'react'
import {View, TouchableOpacity,Text,TextInput,Alert,ListView,ActivityIndicator} from 'react-native'
import {Actions} from 'react-native-router-flux'
import styles from './styles'
import { connect } from 'react-redux'
class Launch extends React.Component {

/* Component life Cycle */
    constructor(props){
      super(props)
      const ds = new ListView.DataSource({rowHasChanged : (r1,r2) => r1 !== r2});
      this.state = { dataSource: ds.cloneWithRows([]), isLoading : false, listEmpty : true};
    }

    componentWillMount() {
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    componentWillReceiveProps(nextProps){
       if (nextProps.ListData) {
         var Rewards = nextProps.ListData['Rewards'];
         if (Rewards.length==0 && this.state.isLoading==true) {
            this.setState({listEmpty:true,isLoading:false});
            Alert.alert('No data found !');
         }
         else{
            this.setState({isLoading : false,listEmpty: false,dataSource : this.state.dataSource.cloneWithRows(Rewards)});
           }
       }
       else if (nextProps.errorMessage){
           Alert.alert('something went wrong please try again later !');
       }
    }

    render(){
        if (this.state.isLoading) {
          return (
             <View  style={[styles.container]}>
               <ActivityIndicator
                style={[styles.centering, {height: 80,backgroundColor: '#eeeeee',width:80}]}
                size="large"/>
             </View>
         );
        } else {
           return(
             <View style={[styles.container]}>
               {this.state.listEmpty &&
                   <TouchableOpacity style={[styles.btn]} onPress = {this.fetchButtonTapped.bind(this)}>
                     <Text>fetch</Text>
                    </TouchableOpacity>
               }
               {
               <ListView
                 dataSource = {this.state.dataSource}
                 renderRow = {(rowData) => this._renderRowData(rowData)}
                 enableEmptySections = {true}/>
               }
               </View>
           );
        }
    }

/* render data method for list */
    _renderRowData(rowData){
      return(
        <View style={styles.CellStyle}>
         <Text style = {styles.textStyle}>{rowData.Title}</Text>
         </View>
      )
    }
 /* handled click of fetch button */
    fetchButtonTapped(){
      this.setState({isLoading : true})
      const {fetchList} = this.props
      const params = {
        PageNumber : 1,
        PageSize : 10,
        CreatedAfetr : null,
        CreatedBefore : null
      }
      fetchList(params);
    }

}
 const mapStateToProps = (state) => {
  return{
    ListData : state.auth.ListData
  }
}
export default connect(mapStateToProps, null)(Launch)
