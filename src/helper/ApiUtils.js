
export const API = 'https://loyalty.collectapps.io/api/v1/rewards?';

class ApiUtils {

 /* API call for fetch list*/
  static getList(params){
    return this.callURL('get', params);
  }

  static callURL(method, params) {
    const {PageNumber,PageSize,CreatedAfetr,CreatedBefore} = params;
    return fetch(`${API}PageNumber=${PageNumber}&PageSize=${PageSize}&CreatedAfetr=${CreatedAfetr}&CreatedBefore=${CreatedBefore}`, {
      method: method,
      mode: 'cors',
      credentials: 'include',
      headers: {
         'Authorization': 'ApiKey hu0BgORHLmFGYbsJpY8vUuSIoa9aBc',
         'Accept': 'application/json',
         'Content-Type': 'application/json',
      },
    }).then((response) => {
      return Promise.all([Promise.resolve(response), response.json()]);
    });
  }
}
export default ApiUtils
