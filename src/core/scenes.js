/* @flow */
import React from 'react'
import {Scene, Actions} from 'react-native-router-flux'
import {LaunchContainer} from '@Launch/containers'

export default Actions.create(
  <Scene key="root">
   <Scene key="launch" component={LaunchContainer} title="Reward List" />
  </Scene>
)
