
import * as actions from './actions';

const initialState = {
   ListData :{},
   errorMessage : ''
};


export default function auth(state = initialState, action) {
  switch (action.type) {
      case actions.FETCHED_LIST:
        return {
          ...state,
          ListData : action.payload
        };
      case actions.FETCH_FAILD:
        return {
          errorMessage : action.message
        };
     default:
      return state;
  };
}
