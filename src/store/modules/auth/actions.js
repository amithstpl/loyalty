import AsyncStorage from '@platform/asyncStorage'
import Permissions from '@platform/store/permissions'
import {Actions} from 'react-native-router-flux'
import ApiUtils from '@Helper/ApiUtils'

// Constants
export const FETCHED_LIST = 'FETCHED_LIST'
export const FETCH_FAILD = 'FETCH_FAILD'

export function fetchList(data : Object) {
  return function(dispatch) {
   return ApiUtils.getList(data)
     .then(([response, user]) => {
       var responseData=JSON.parse(response._bodyText);
       dispatch(fetchSuccess(responseData))
     }).catch(err => {
       dispatch(fetchFailure(err.message));
     });
 };

}

export function fetchSuccess(data: Object){
  return{
    type : FETCHED_LIST,
    payload : data
  }
}

export function fetchFailure(error){
  return{
    type : FETCH_FAILD,
    message : error
  }
}
