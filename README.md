#Loyalty

React Native example app to show the rewards list using Redux and Router flux framework.

##To run this example:

1. Download this repo or git clone https://amithstpl@bitbucket.org/amithstpl/loyalty.git
From the repo folder run:
2. npm install
3. npm start
4. Run project through XCode or Android Studio

And also head over to http://redux.js.org/ for some great documentation.
