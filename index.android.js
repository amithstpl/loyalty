/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import { AppRegistry } from 'react-native'
 import Kernel from '@core'

AppRegistry.registerComponent('Loyalty', () => Kernel);
